INSERT INTO person (id, first_name, last_name, age)
    VALUES (0, "Zed", "Shaw", 37);
	
INSERT INTO person (id, first_name, last_name, age)
	VALUES (1, "Jose", "Delgado", 23);

INSERT INTO pet (id, name, breed, age, dead)
    VALUES (0, "Fluffy", "Unicorn", 1000, 0);

INSERT INTO pet VALUES (1, "Gigantor", "Robot", 1, 1);

INSERT INTO pet VALUES (2, "Oreo", "Mixed", 1, 0);