/* make sure there's dead pets */
SELECT name, age FROM pet WHERE dead = 1;

/* aww poor robot */
DELETE FROM pet WHERE dead = 1;

/* make sure the robot is gone */
SELECT * FROM pet;

/* let's resurrect the robot */
INSERT INTO pet VALUES (1, "Gigantor", "Robot", 1, 0);

/* the robot LIVES! */
SELECT * FROM pet;

/* Delete Oreo from the DB */
DELETE FROM pet WHERE name = "Oreo";

/* make sure Oreo is gone */
SELECT * FROM pet;

/* let's resurrect the Oreo Cookie */
INSERT INTO pet VALUES (2, "Oreo", "Mixed Cookie", 1, 0);

/* Oreo is double stuffed ALIVE! */
SELECT * FROM pet;