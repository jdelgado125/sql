/* Find all pets that belong to Zed,
 * Then delete those pets 
 */
DELETE FROM pet WHERE id IN (
    SELECT pet.id
    FROM pet, person_pet, person
    WHERE
    person.id = person_pet.person_id AND
    pet.id = person_pet.pet_id AND
    person.first_name = "Zed"
);

SELECT * FROM pet; /* Return all pets information */
SELECT * FROM person_pet; /* Return the ownership of pets */

/* Check if there are any pets,
 * Delete every pet now found in the pet table
 */
DELETE FROM person_pet WHERE pet_id NOT IN (
        SELECT id FROM pet
);

/* Display all pet and person relations */
SELECT * FROM person_pet;