SELECT * FROM person;

SELECT name, age FROM pet;

SELECT name, age FROM pet WHERE dead = 0;

SELECT * FROM person WHERE first_name != "Zed";

SELECT name, age FROM pet WHERE age > 10;

SELECT * FROM person WHERE age > 23;

SELECT * FROM person WHERE age < 23;

SELECT * FROM person WHERE first_name = "Jose" AND age > 20;

SELECT name FROM pet WHERE dead = 0 AND age > 0 OR breed != "Robot";